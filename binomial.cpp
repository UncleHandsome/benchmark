//////////////////////////////////////////////////////////////////////////////
//// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
//// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//// PARTICULAR PURPOSE.
////
//// Copyright (c) Microsoft Corporation. All rights reserved
//////////////////////////////////////////////////////////////////////////////
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll
// RUN: mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t
// RUN: %embed_kernel kernel.cl kernel.o
// RUN: popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out

//----------------------------------------------------------------------------
// File: BinomialOptions.cpp
//
// Implement GPU based binomial option pricing. Verify correctness with CPU
// implementation
//----------------------------------------------------------------------------

#include <vector>
#include <assert.h>
#include <iostream>
#include <chrono>
#include <ctime>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <CL/opencl.h>
#include <math.h>


#pragma warning (disable : 4267)

// Date set - small and normal
// small problem size
//#define  MAX_OPTIONS    (32)
//#define  NUM_STEPS      (64)
//#define  TIME_STEPS     (2)
//#define  CACHE_DELTA    (2 * TIME_STEPS)
//#define  CACHE_SIZE     (16)
//#define  CACHE_STEP     (CACHE_SIZE - CACHE_DELTA)

//// normal problem size
#define  MAX_OPTIONS    (512)
#define  NUM_STEPS      (2048)
#define  TIME_STEPS     (16)
#define  CACHE_DELTA    (2 * TIME_STEPS)
#define  CACHE_SIZE     (256)
#define  CACHE_STEP     (CACHE_SIZE - CACHE_DELTA)

typedef struct _option_data
{
    float s;
    float x;
    float t;
    float r;
    float v;
} t_option_data;

#if NUM_STEPS % CACHE_DELTA
#error Bad constants
#endif

//----------------------------------------------------------------------------
// Generate random data between specified values
//----------------------------------------------------------------------------
float random_data(float lo, float hi)
{
    int len = (int) ::ceil(hi - lo);
    return lo + ::rand() % len;
}

//----------------------------------------------------------------------------
// GPU/CPU implementation - Call value at period t : V(t) = S(t) - X
//----------------------------------------------------------------------------
float expiry_call_value(float s, float x, float vdt, int t)
{
    float d = s * exp(vdt * (2.0f * t - NUM_STEPS)) - x;
    return (d > 0) ? d : 0;
}

static char* load_program_source(const char *filename)
{
    struct stat statbuf;
    FILE        *fh;
    char        *source;

    fh = fopen(filename, "r");
    if (fh == 0)
        return 0;

    stat(filename, &statbuf);
    source = (char *) malloc(statbuf.st_size + 1);
    fread(source, statbuf.st_size, 1, fh);
    source[statbuf.st_size] = '\0';

    return source;
}

//----------------------------------------------------------------------------
// Sequential(CPU) binomial option calculation
//----------------------------------------------------------------------------
void binomial_options_cpu(std::vector<float>& v_s, std::vector<float>& v_x,
                          std::vector<float>& v_vdt, std::vector<float>& v_pu_by_df,
                          std::vector<float>& v_pd_by_df,
                          std::vector<float>& call_value)
{
    const unsigned data_size = v_s.size();
    // all data size should be same
    assert(data_size == MAX_OPTIONS); // kernel uses this macro directly
    assert((v_s.size() == data_size) && (v_x.size() == data_size) &&  (v_vdt.size() == data_size) &&
           (v_pu_by_df.size() == data_size) && (v_pd_by_df.size() == data_size) && (call_value.size() == data_size));

    // this is like GPU kernel - where we have the meat
    for (unsigned i = 0; i < data_size; i++)
    {
        float call[NUM_STEPS + 1];

        // Compute values at expiration date:
        // call option value at period end is V(T) = S(T) - X
        // if S(T) is greater than X, or zero otherwise.
        // The computation is similar for put options.
        for(int j = 0; j <= NUM_STEPS; j++)
            call[j] = expiry_call_value(v_s[i], v_x[i], v_vdt[i], j);

        // Walk backwards up binomial tree
        for(int j = NUM_STEPS; j > 0; j--)
            for(int k = 0; k <= j - 1; k++)
                call[k] = v_pu_by_df[i] * call[k + 1] + v_pd_by_df[i] * call[k];

        call_value[i] = call[0];
    }
}

//----------------------------------------------------------------------------
// Wrapper offloading computation to GPU
// This function calls C++ AMP binomial option kernel
//----------------------------------------------------------------------------
cl_uint          num_platforms;
cl_platform_id   platform_id[10];
cl_device_id     device_id;
cl_context       context;
cl_command_queue queue;
void init()
{
    int err;
    err = clGetPlatformIDs(10, platform_id, &num_platforms);
    for (int i = 0; i < num_platforms; i++) {
        err = clGetDeviceIDs(platform_id[i], CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
        if (err != CL_SUCCESS)
            continue;
        else
            break;
    }
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    queue = clCreateCommandQueue(context, device_id, 0, &err);
}
void binomial_options_gpu(std::vector<float>& v_s, std::vector<float>& v_x,
                          std::vector<float>& v_vdt, std::vector<float>& v_pu_by_df,
                          std::vector<float>& v_pd_by_df,
                          std::vector<float>& call_value)
{
    const unsigned data_size = v_s.size();
    // all data size should be same
    assert(data_size == MAX_OPTIONS); // kernel uses this macro directly
    assert((v_s.size() == data_size) && (v_x.size() == data_size) &&  (v_vdt.size() == data_size) &&
           (v_pu_by_df.size() == data_size) && (v_pd_by_df.size() == data_size) && (call_value.size() == data_size));

    cl_kernel        kernel;
    cl_program       program;

    int err;

    cl_mem d_v_s;
    cl_mem d_v_x;
    cl_mem d_v_vdt;
    cl_mem d_v_pu_by_df;
    cl_mem d_v_pd_by_df;
    cl_mem d_call_value;
    cl_mem d_call_buffer;

    std::vector<int> call_buffer(MAX_OPTIONS*(NUM_STEPS + 16));

    d_v_s = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                           sizeof(float) * data_size, v_s.data(), &err);
    d_v_x = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                           sizeof(float) * data_size, v_x.data(), &err);
    d_v_vdt = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                             sizeof(float) * data_size, v_vdt.data(), &err);
    d_v_pu_by_df = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                                  sizeof(float) * data_size, v_pu_by_df.data(), &err);
    d_v_pd_by_df = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                                  sizeof(float) * data_size, v_pd_by_df.data(), &err);

    d_call_value = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                                  sizeof(float) * data_size, call_value.data(), &err);
    d_call_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                                   sizeof(float) * MAX_OPTIONS * (NUM_STEPS + 16), call_buffer.data(), &err);

    char *source = load_program_source("binomial.cl");
    program = clCreateProgramWithSource(context, 1, (const char **) & source, NULL, &err);
    assert(err == CL_SUCCESS);
    err = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
    assert(err == CL_SUCCESS);
    kernel = clCreateKernel(program, "binomial_options_kernel", &err);
    free(source);

    err  = clSetKernelArg(kernel,  0, sizeof(cl_mem), &d_v_s);
    err |= clSetKernelArg(kernel,  1, sizeof(cl_mem), &d_v_x);
    err |= clSetKernelArg(kernel,  2, sizeof(cl_mem), &d_v_vdt);
    err |= clSetKernelArg(kernel,  3, sizeof(cl_mem), &d_v_pu_by_df);
    err |= clSetKernelArg(kernel,  4, sizeof(cl_mem), &d_v_pd_by_df);
    err |= clSetKernelArg(kernel,  5, sizeof(cl_mem), &d_call_value);
    err |= clSetKernelArg(kernel,  6, sizeof(cl_mem), &d_call_buffer);

    size_t global[1], local[1];
    global[0] = MAX_OPTIONS * CACHE_SIZE;
    local[0] = CACHE_SIZE;
    err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, global, local, 0, NULL, NULL);
    clFinish(queue);

    clReleaseMemObject(d_v_s);
    clReleaseMemObject(d_v_x);
    clReleaseMemObject(d_v_vdt);
    clReleaseMemObject(d_v_pu_by_df);
    clReleaseMemObject(d_v_pd_by_df);
    clReleaseMemObject(d_call_value);
    clReleaseMemObject(d_call_buffer);
    clReleaseContext(context);
    clReleaseKernel(kernel);
    clReleaseProgram(program);
    clReleaseCommandQueue(queue);
}
//----------------------------------------------------------------------------
// Generated random data is normalized to conform for Binomial calculation
//----------------------------------------------------------------------------
void normalize_data(t_option_data* option_data, int num_options,
                    std::vector<float>& v_s, std::vector<float>& v_x, std::vector<float>& v_vdt,
                    std::vector<float>& v_pu_by_df, std::vector<float>& v_pd_by_df)
{
    for (int i = 0; i < num_options; i++)
    {
        v_s[i] = option_data[i].s;
        v_x[i] = option_data[i].x;

        float t = option_data[i].t;
        float r = option_data[i].r;
        float v = option_data[i].v;

        float dt = t / (float)NUM_STEPS;
        v_vdt[i] = v * ::sqrt(dt);
        float rdt = r * dt;

        //Per-step interest and discount factors
        float iff = ::exp(rdt);
        float df = ::exp(-rdt);

        //Values and pseudoprobabilities of upward and downward moves
        float u = ::exp(v_vdt[i]);
        float d = ::exp(-v_vdt[i]);
        float pu = (iff - d) / (u - d);
        float pd = 1.0f - pu;
        v_pu_by_df[i] = pu * df;
        v_pd_by_df[i] = pd * df;
    }
}


int main()
{
    int num_options = MAX_OPTIONS;

    std::vector<float> call_value_gpu(num_options);
    std::vector<float> call_value_cpu(num_options);
    t_option_data option_data[MAX_OPTIONS];

    //Generate options set
    srand(123);
    for(int i = 0; i < num_options; i++)
    {

        option_data[i].s = random_data(5.0f, 30.0f);
        option_data[i].x = random_data(1.0f, 100.0f);
        option_data[i].t = random_data(0.25f, 10.0f);
        option_data[i].r = 0.06f;
        option_data[i].v = 0.10f;
    }

    std::vector<float> v_s(num_options);
    std::vector<float> v_x(num_options);
    std::vector<float> v_vdt(num_options);
    std::vector<float> v_pu_by_df(num_options);
    std::vector<float> v_pd_by_df(num_options);

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds;



    normalize_data(option_data, num_options, v_s, v_x, v_vdt, v_pu_by_df, v_pd_by_df);

    printf ("CPU computation...");
    start = std::chrono::system_clock::now();
    binomial_options_cpu(v_s, v_x, v_vdt, v_pu_by_df, v_pd_by_df, call_value_cpu);
    end = std::chrono::system_clock::now();
    printf("done\n");
    elapsed_seconds = end - start;
    std::cout << "CPU executation time: " << elapsed_seconds.count() << std::endl;

    printf ("GPU computation...");
    init();
    start = std::chrono::system_clock::now();
    binomial_options_gpu(v_s, v_x, v_vdt, v_pu_by_df, v_pd_by_df, call_value_gpu);
    end = std::chrono::system_clock::now();
    elapsed_seconds = end - start;
    printf("done\n");
    std::cout << "GPU executation time: " << elapsed_seconds.count() << std::endl;

    double sum_delta = 0, sum_ref = 0, error_val = 0;
    printf("\nCPU binomial vs. GPU binomial\n");
    for (int i = 0; i < num_options; i++)
    {
        sum_delta += ::fabs(call_value_gpu[i] - call_value_cpu[i]);
        sum_ref += call_value_gpu[i];
    }
    if(sum_ref > 1E-5)
        printf("L1 norm: %E\n", error_val = sum_delta / sum_ref);
    else
        printf("Avg. diff: %E\n", error_val = sum_delta / (double)num_options);
    printf((error_val < 5e-4) ? "Data match\n" : "Data doesn't match\n");
    return error_val >= 5e-4;
}


