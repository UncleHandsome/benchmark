// Date set - small and normal
// small problem size
//#define  MAX_OPTIONS    (32)
//#define  NUM_STEPS      (64)
//#define  TIME_STEPS     (2)
//#define  CACHE_DELTA    (2 * TIME_STEPS)
//#define  CACHE_SIZE     (16)
//#define  CACHE_STEP     (CACHE_SIZE - CACHE_DELTA)

//// normal problem size
#define  MAX_OPTIONS    (512)
#define  NUM_STEPS      (2048)
#define  TIME_STEPS     (16)
#define  CACHE_DELTA    (2 * TIME_STEPS)
#define  CACHE_SIZE     (256)
#define  CACHE_STEP     (CACHE_SIZE - CACHE_DELTA)

float expiry_call_value(float s, float x, float vdt, int t)
{
    float d = s * exp(vdt * (2.0f * t - NUM_STEPS)) - x;
    return (d > 0) ? d : 0;
}

__kernel void binomial_options_kernel(__global float* s,
                                    __global float* x,
                                    __global float* vdt,
                                    __global float* pu_by_df,
                                    __global float* pd_by_df,
                                    __global float* call_value,
                                    __global float* call_buffer)
{
    int tile_idx = get_group_id(0);
    int tid = get_local_id(0);
    int i;

    __local float call_a[CACHE_SIZE + 1];
    __local float call_b[CACHE_SIZE + 1];

    for (i = tid; i <= NUM_STEPS; i += CACHE_SIZE)
    {
        int idx = tile_idx * (NUM_STEPS + 16) + (i);
        call_buffer[idx] = expiry_call_value(s[tile_idx], x[tile_idx], vdt[tile_idx], i);
    }

    for (i = NUM_STEPS; i > 0; i -= CACHE_DELTA)
    {
        for (int c_base = 0; c_base < i; c_base += CACHE_STEP)
        {
            int c_start = min(CACHE_SIZE - 1, i - c_base);
            int c_end   = c_start - CACHE_DELTA;

            barrier(CLK_LOCAL_MEM_FENCE);
            if(tid <= c_start)
			{
				int idx = tile_idx * (NUM_STEPS + 16) + (c_base + tid);
                call_a[tid] = call_buffer[idx];
			}

            for(int k = c_start - 1; k >= c_end;)
			{
                barrier(CLK_LOCAL_MEM_FENCE);
                call_b[tid] = pu_by_df[tile_idx] * call_a[tid + 1] + pd_by_df[tile_idx] * call_a[tid];
                k--;

                //Compute discounted expected value
                barrier(CLK_LOCAL_MEM_FENCE);
                call_a[tid] = pu_by_df[tile_idx] * call_b[tid + 1] + pd_by_df[tile_idx] * call_b[tid];
                k--;
            }

            //Flush shared memory cache
            barrier(CLK_LOCAL_MEM_FENCE);
            if(tid <= c_end)
			{
				int idx = tile_idx * (NUM_STEPS + 16) + (c_base + tid);
                call_buffer[idx] = call_a[tid];
			}
        }
    }
    
    if (tid == 0)
        call_value[tile_idx] = call_a[0];
}
